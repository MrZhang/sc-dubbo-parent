package com.sd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScDubboCloudApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScDubboCloudApplication.class, args);
    }

}
